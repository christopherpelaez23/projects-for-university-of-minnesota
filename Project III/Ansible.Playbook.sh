- hosts: update wordpress
  connection: local
  tasks:
    - name: stop httpd
      systemd:
        name: httpd
        state: stopped
      become: true

    - name: backup html files
      archive:
        path: /var/www/html
        dest: "/home/centos/backups/wordpress-bck-{{ansible_date_time.iso8601_basic_short}}.tgz"
        format: gz
      become: true

    - name: backup wordpress database
      command: /etc/backup-wpdb.sh
      become: true

    - name: get latest wordpress
      unarchive:
        src:  https://wordpress.org/latest.zip
        dest: /tmp/
        remote_src: yes
      become: true

    - name: Wait until wordpress has been downloaded
      wait_for:
        path: /tmp/wordpress/index.php
        state: present

    - name: copy wordpress to website
      shell: /bin/cp -rf /tmp/wordpress/* /var/www/html/
      become: true

    - name: delete tmp wordpress
      file:
        path: /tmp/wordpress
        state: absent
      become: true

    - name: start httpd
      systemd: 
        name: httpd
        state: started
        daemon_reload: yes
      become: true
