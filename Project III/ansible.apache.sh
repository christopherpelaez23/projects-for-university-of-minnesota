- name: Update mysql
  hosts: webservers
  tasks:
    - name: stop mysql
      systemd:
        name: mysql 
        state: stopped
      become: true

    - name: Download mysql 
        command: curl -L -O https://dev.mysql.com/get/mysql-apt-config_0.8.22-1_all.deb

    - name: install mysql
      command: dpk -i mysql-apt-config_0.8.22-1_all.deb
      
    - name: update package information
        shell: apt-get update
    
    - name: update mysql server
      command: apt-get install mysql-server

    - name: restart mysql
      service: 
      name: mysql
      state: started
      become: true

    
