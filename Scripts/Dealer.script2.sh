#!/bin/bash

echo
clear
echo -n "Please insert the Specific Time: "
read time
echo
echo -n "Please insert the Specific Date (4 digit): "
read dates

echo
echo "From file "$dates"_Dealer_schedule"
echo
grep -iw $time "$dates"_Dealer_schedule | awk '{print $1, $2, $3, $4}'
