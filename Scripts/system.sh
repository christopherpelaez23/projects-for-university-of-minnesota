#!/bin/bash

echo "starting Script"

# free memory
free -h > Projects/backups/freemem/free_mem.txt

#  disk  Usages
du -ch  > Projects/backups/diskuse/disk_usage.txt

#   All open File
lsof -i > Projects/backups/openlist/open_list.txt


#  system disk space
df -h > Projects/backups/freedisk/free_disk.txt
