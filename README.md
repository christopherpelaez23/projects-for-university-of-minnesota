# ELK Stack Deployment
Base on the following Configuration network I Used the next diagram indicate below:
Link:https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Network%20Diagram/Diagram_cloud_2.png

# Following step I used to configure and install Docker:
The following file locate in Project I Folder contains 3 file: *Install_ELK   *Filebeat_Config   *MetricFilebeat 
This three file contains basically the configurantion and installation to run those script using Docker Container.
https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Scripts/install_Docker.sh

# The following command show step to configuartion docker "installation Process"
 To get started used the following Command docker /ansible:
-	sudo apt install docker.io
-	sudo systemctl status docker
-	sudo systemctl start docker
-   sudo su (Get root access and probe container)
- Full Process locate in folder Script called "Install"Docker"
- Note: the main Configuration locate in cd /etc/ansible/

# Description of the Topology
The main function of this network here is design a load-balanced using Dvwa To see any Vulnerable Web Application.
Load balancers are used to provide availability and scalability to the application. distributes the incoming traffic to ensure that there is continued availability to the web-server, in addition to restricting access to the network.

The main advantage using a Redteam-vm is to allow easy administration of many systems and to provide an extra layer of security.

Another resources integrated main sevices called ELK server allows users to easily monitor the vulnerable VMs for changes to the event logs and system metrics.

- Filebeat monitors specific log directories or log files, collects information and then logs the data.

- Metricbeat collects metrics and statistics and sends them to things such as Logstash. The ouput data can then be viewed in things such as Kibana.

The  Following configuration details of each machine may be found below:

| Names        |     Function         |  IP Address |            Operating System|  
| ------------- |:-------------:| -----:| -------------------:|
|   Redteam_VM  | Gateway | 10.0.0.4 |     Linux (ubuntu 18.04 LTS)                   
| Web-1     |    Web Server - DVWA - Docker   |10.0.0.7   | Linux (ubuntu 18.04 LTS) 
| Web-2     |  Web Server - DVWA - Docker     |10.0.0.8   |Linux (ubuntu 18.04 LTS) 
|Elk-Server |ELK Stack                        |10.1.0.4   |Linux (ubuntu 18.04 LTS) 


 # Access Policies

 The machine Red-team access conection only using personal Public ip  but the internal network are not exposed to public internet.
Machines within the network can only be accessed by Redteam_vm access. The ELK-Server is only accessible by SSH from the Red-team_vm and through web access to Personal IP address.

A summary of the access policies in place can be found in the table below:

| Name      |    Public Access       | Allowed IP Address |
| ------------- |:-------------:| -----:|
| Red-Team_VM   | Yes       |          Personal IP    |
|  Web-1    | no            |   10.0.0.4 10.0.0.7|
|  Web-2    | no            |    10.0.0.4 10.0.0.8 |
|ELK-Server| yes            | 10.0.0.4 |
|RedTeam-LB| Yes            |Personal IP|

# Elk Configuration
The Ansible container was install  in the ELK machine. base on that I used the following configuration called in the folder project I "ElK Config_Install.yml"

Description Script Container:
* Install docker.io 
* install pip3 .
* Docker module.
* Increases the virtual memory.
* Downloads and launches the docker ELK container.
* Enable docker to start on boot.

Link bellow show how to install process :https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Project%20I%20/ElK%20Config_Install.yml



# Target Machines & Beats
This ELK server is configured to monitor the following machines:

Web-1: 10.0.0.7

Web-2: 10.0.0.8

I have installed the following Beats on these machines:

* Filebeat
* Metricbeat

-These Beats allow us to collect the following information from each machine.

* Filebeat collects auth logs (/var/log/auth.log), which can be used to monitor access attempts.
Metricbeat collects metrics related to CPU, memory and running proccessing, which can be used to optimize the computer speed and efficiency and detect any unwanted breaches.

# Using the Playbook

here I configured the file called "host" in the project I. here I make to default private ip configuration:
Update the /etc/ansible/hosts file to include the internal IP addresses.
* Update first Host link bellow show:
https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Project%20I%20/host%20Ansible%20Docker

* Run the playbook make sure docker start, and navigate to http://[your.ELK-VMExternal.IP]:5601/app/kibana to check that the installation worked as expected.


# The Filebeat Install and Configuration
Here I show the configuration  file called "install_Filebeat" in the project I. basicaly contains the file to install filebeat and  the configuration to run filebeat on kibana.
link bellow to install filebeat:

* https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Project%20I%20/Filebeat_Conf_Playbook.yml

To download the configuration direct by the website using terminal following command:
* curl https://gist.githubusercontent.com/slape/5cc350109583af6cbe577bbcc0710c93/raw/eca603b72586fbe148c11f9c87bf96a63cb25760/Filebeat >> /etc/ansible/filebeat-config.yml.


#  MetricsBeats Install and Configuration: 
Here I show the configuration for the  file called "install_metricbeeats" in the project I. basicaly explain 2 file here to install and make configuration to install metricbeats to run on elk Server.

* Update the /etc/ansible/hosts file.
* link bellow show the installation Process:

https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Project%20I%20/metricbeat-playbook.yml

The file configuration is bellow this link:

https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Project%20I%20/Metricbeat_Conf.yml

# Steps to setup ELK server
* Open command prompt
* ssh username@RedTEAM_VM {PrivateIP}
* sudo docker container list -a (to see what is running)
* Locate ansible container name
* sudo docker start (name of container)
* sudo docker attach (name of container) or sudo docker exec -it (name of container) bash (this one will stay open even when exit so it is good if you are going back and forth between containers)
* -cd /etc/ansible/
* Update hosts file with ELK IP and change the remote user to your chosen one in /etc/ansible/hosts
[elk]
* ansible-playbook install-elk.yml install-ELK
* ssh username@ELK-serverPrivateIP

# Steps to setup Filebeat & MetricsBeats
 * Open command prompt
* ssh username@ELK-serverPrivateIP
* using command cp etc/ansible/metricbeat-config.yml /etc/metricbeat/metricbeat-config.yml (copy file so there is backup)
* Using nano create new file called install_Filebeat and save
* Using nano create file  called  playbook_install and save
* After entering your information into the Filebeat configuration file and Ansible playbook, you should have run: ansible-playbook filebeat-playbook.yml to test all  file Scripts before located in the Project I folder .
Run the docker make sure is working : https://gitlab.com/christopherpelaez23/projects-for-university-of-minnesota/-/blob/main/Kibana_Setup/Docker_conf.png

# Summary and guide complete:
 conclusion run all script mention before make sure with have all this file bellow :
 location in :cd /etc/ansible
* ansible-playbook install_elk.yml
* ansible-playbook install_filebeat.yml for   webservers 1
* ansible-playbook install_metricbeat.yml for webservers 2
* Link for Google drive document Copy :  https://docs.google.com/document/d/1meN-O8uvYfF-qm2XTdvYvDklv7kx5Q1R/edit?usp=sharing&ouid=114013463614419364753&rtpof=true&sd=true



